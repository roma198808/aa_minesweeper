class Game
  attr_reader :comp_board, :user_board

  def initialize
    @board = Board.new(10)
  end

  def run
    10.times do
      puts "What's your move?"
      input  = gets.chomp

      action = input[0]
      y  = input[4].to_i
      x = input[2].to_i

      current_tile = @board.tiles[x][y]

      value = @board.user_move(action,current_tile,@board)

      if value == "Game Over"
        puts "Game Over"
        break
      end

      display_board
    end
  end

  def display_board
    @board.display
    puts
  end

end

class Board
  attr_reader :rows
  attr_accessor :tiles
  attr_accessor :comp_board

  def initialize(num_bombs)
    @num_bombs  = num_bombs
    @tiles      = create_tiles
    @comp_board = []
    create_comp_board
  end

  def create_tiles
    board = []

    9.times do |i|
      row = Array.new
      9.times do |j|
        row << Tile.new(j,i) # j index is x value and i index is y value
      end
      board << row
    end

    board
  end

  def create_comp_board
    9.times do
      row = Array.new
      9.times do
        row << 0
      end
      @comp_board << row
    end

    put_bombs
    process_rows
  end

  def put_bombs
    @num_bombs.times do
      put_bomb
    end
  end

  def put_bomb
    cell_i  = rand(9)
    row_i = rand(9)

    @comp_board[row_i][cell_i] == "b" ? put_bomb : @comp_board[row_i][cell_i] = "b"
  end

  def process_rows
    @comp_board.each_with_index do |row,row_i|
      row.each_with_index do |cell,cell_i|
        process_cell(row_i,cell_i)
      end
    end
  end

  def process_cell(cell_i,row_i)
    unless @comp_board[row_i][cell_i] == "b"

      if row_i > 0 && cell_i > 0 && @comp_board[row_i-1][cell_i-1] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if cell_i > 0 && @comp_board[row_i][cell_i-1] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if row_i < 8 && cell_i > 0 && @comp_board[row_i+1][cell_i-1] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if row_i > 0 && @comp_board[row_i-1][cell_i] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if row_i < 8 && @comp_board[row_i+1][cell_i] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if row_i > 0 && cell_i < 8 && @comp_board[row_i-1][cell_i+1] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if cell_i < 8 && @comp_board[row_i][cell_i+1] == "b"
        @comp_board[row_i][cell_i] += 1
      end

      if row_i < 8 && cell_i < 8 && @comp_board[row_i+1][cell_i+1] == "b"
        @comp_board[row_i][cell_i] += 1
      end

    end
  end

  def comp_display
    puts
    @comp_board.each do |row|
      puts row.join(' ')
    end
  end

  def display
    puts
    @tiles.each do |row|
      cell_change = row.map { |tile| tile.sym }
      puts cell_change.join(' ')
    end
  end

  def user_move(action, tile, board)
    if action == "f"
      @tiles[i][j].flag

    elsif action == "r"
      current_value = board.comp_board[tile.x][tile.y]

      if current_value == "b"
        return "Game Over"

      elsif current_value > 0
        tile.sym = current_value

      else
        process_zero(tile,board)
      end
    end
  end

  def process_zero(tile,board)
    tiles_to_process = [ tile ]

    until tiles_to_process.empty?
      current_tile = tiles_to_process.shift

      current_tile.neighbors(board).each do |neighbor|
        current_value = board.comp_board[current_tile.x][current_tile.y]

        if current_value > 0
          tile.sym = current_value.to_s
        elsif current_value == 0 && current_tile.sym == :*
          process_zero(neighbor, board)
        end

      end
    end
  end

end

class Tile
  attr_accessor :x, :y, :sym

  def initialize(x,y)
    @x = x
    @y = y
    @sym = :*
  end

  def flag
    if @sym == :f
      @sym = :*
    elsif @sym == :*
      @sym = :f
    end
  end

  def neighbors(board)
    potential_coords = [ [@x, @y+1], [@x, @y-1], [@x+1, @y], [@x-1, @y],
    [@x+1, @y+1], [@x-1, @y+1], [@x+1, @y-1], [@x-1, @y-1]]

    coords = potential_coords.select do |coord|
      coord[0] >= 0 && coord[0] <= 8 && coord[1] >= 0 && coord[1] <= 8
    end

    neighbor_tiles = []

    coords.each do |coord|
      neighbor_tiles << board.tiles[coord[0]][coord[1]]
    end

    neighbor_tiles
  end
end
g = Game.new
g.run
